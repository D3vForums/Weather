# Weather
> Open Source PHP Project for a Weather Accuracy Website.

![](screenshot.png)

## Installation

OS X & Linux:

```sh
Coming Soon
```

Windows:

```sh
Coming Soon
```

## Usage 

This Project can be used for your own weather site, instead of using the slow government controlled ones. 

_For more examples and usage, please refer to the [Wiki][wiki]._

## Release History

* 0.0.1
    * Work in progress

## Meta

Brooke Moore /-/ [Website](https://chaottiic.com) /-/ [Twitter](https://twitter.com/Chaottiic)

Distributed under the MIT license. See ``LICENSE`` for more information.

## Contributing

1. Fork it (<https://gitlab.com/D3vForums/Weather/forks/new>)
2. Create your feature branch (`git checkout -b feature/Weather`)
3. Commit your changes (`git commit -am 'Add some Weather'`)
4. Push to the branch (`git push origin feature/Weather`)
5. Create a new Pull Request

<!-- Markdown link & img dfn's -->
[wiki]: https://gitlab.com/D3vForums/Weather/wikis/home
